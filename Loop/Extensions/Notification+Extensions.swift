//
//  Notification+Extensions.swift
//  Loop
//
//  Created by Kai Azim on 2023-06-14.
//

import Foundation

extension Notification.Name {
    static let currentDirectionChanged = Notification.Name("currentResizingDirectionChanged")
    static let closeLoop = Notification.Name("closeLoop")
}
